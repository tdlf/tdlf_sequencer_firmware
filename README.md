### Firmware for ESP32 touch capacitive sequencer, links with the technologies de la fête server to control CV and midi instruments.

### The representation of notes, steps, midi channel, etc.
```
device n  |-- voice (0-7)
          |-- midi channel (1-16)
          |-- bar (0-3)
          |-- midi note number (ex. bass drum == 36)
          |-- mute state (on/off)
          |-- step[64]|
                         |-- on/off
                         |-- note length
                         |-- note velocity
                         |-- ratch
```
The sequence information is kept in a 8 value array of type 'device'. A device is a struct containing the voice number, midi channel, bar, midi note number, mute step information as well as an array of 64 steps, themselves a 'step' struct containing note on or off value, note length, note velocity and note ratch information.
```
///// step /////
typedef struct {
    bool on;          
    uint8_t length;
    uint8_t vel;
    uint8_t ratch;
} steps_t;

///// device /////
typedef struct {
    uint8_t voice;
    uint8_t chan;
    uint8_t bar;
    uint8_t note;
    bool mute;
    steps_t steps[64];
} device_t;

device_t devices[8];
```
### Receiving sequences

Handling incoming messages via UDP is done via the ESParam library, itself using the microOSC library https://github.com/thomasfredericks/MicroOsc
The myBlobMessageParser( MicroOscMessage& receivedOscMessage ) function is called when a blob  (byte array) is received
The blob will represent a sequence as a device. To get the bar of the given device, do 'myBar = devices[0].bar'

### Sending sequences

The updateUdpServer() function is called when a new short press event occurs.  The changed sequence is sent as a blob to the server.  

myMicroOscUdp.sendBlob("/tdlf/sequence", unsigned char *b, int32_t length)




