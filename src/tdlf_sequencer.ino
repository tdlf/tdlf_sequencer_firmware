#include <FastLED.h>
#include <ESParam.h>
#include <ESParamPrinter.h>
#include <ESPmDNS.h>
#include <WiFi.h>
#include <cstdint>
#include <cstring>

///// step /////
typedef struct {
    bool on;          
    uint8_t length;
    uint8_t vel;
    uint8_t ratch;
} steps_t;

///// device /////
typedef struct {
    uint8_t voice;
    uint8_t chan;
    uint8_t bar;
    uint8_t note;
    uint8_t mute;
    steps_t steps[64];
} device_t;

device_t devices[8];

uint8_t curVoice = 0;

typedef enum {
    STOPPED = 0,
    WILL_PLAY = 1,
    PLAYING = 2
};

//#define LED_DATA_PIN 23 // for APA102
//#define LED_CLOCK_PIN
#define LED_DATA_PIN  18 // for WS2812B
#define COLOR_ORDER GRB
#define NUM_LEDS 16
CRGB leds[NUM_LEDS];

// LED animation stuff
ColorParam colorA;
ColorParam colorB;
IntParam brightnessParam;

// Sequencer params
IntParam playParam;
IntParam setStepParam;
FloatParam bpmParam;
TextParam seqParam;

IntParam touchDownTresh;

// Devices specific params
IntParam curVoiceParam; // The number of the sequencer
IntParam midiChannelParam;
IntParam midiNoteParam; // 36 = bass drum 
       
// sequencer stuff
#define STEP_COUNT 16
#define STEPS 64
// touch stuff
const uint8_t TOUCH_PINS[STEP_COUNT][2] = {
    {2,13},
    {4,13},
    {15,13},
    {32,13},
    {2,12},
    {4,12},
    {15,12},
    {32,12},
    {2,14},
    {4,14},
    {15,14},
    {32,14},
    {2,27},
    {4,27},
    {15,27},
    {32,27}
};

// using an enter threshold and a leave threshold instead of a timed debounce.
#define TOUCH_DOWN_THRESH 25 // 13 
#define TOUCH_UP_THRESH 27
#define SHORT_PRESS_MS 10
#define LONG_PRESS_MS 777

CRGB touchDownColor = CRGB(10,0,0);
CRGB touchUpColor = CRGB(0,0,10);
CRGB longPressColor = CRGB(0,10,0);
CRGB stepColor = CRGB(30,30,30); 
CRGB stepOffColor = CRGB(0,0,0);
CRGB mutedColor = CRGB(2,2,2);
CRGB stepOnColor; 

CRGB colors[] = {
  CRGB(14, 5, 7), // rose
  CRGB(13, 0, 0), // rouge
  CRGB(14, 6, 0), // orange
  CRGB(14, 14, 0), // jaune
  CRGB(0, 6, 0), // vert
  CRGB(0, 7, 13), // bleu clair
  CRGB(3, 0, 13), // bleu foncé
  CRGB(11, 0, 11) // mauve
};

typedef enum {
    TOUCH_DOWN,
    TOUCH_UP,
    SHORT_PRESS,
    LONG_PRESS
} TouchEvent;

const char * touchEventNames[] = {
    "TOUCH_DOWN",
    "TOUCH_UP",
    "SHORT_PRESS",
    "LONG_PRESS"
};

typedef struct {
    bool state;
    uint8_t pinA;
    uint8_t pinB;
    unsigned long touch_down_ts;
    CRGB color;
} TouchThis;
TouchThis touchSensors[STEP_COUNT];

uint8_t live = 0;
uint8_t curBar = -1;
uint8_t curStep;
uint8_t curBeat;
uint8_t oldBeat = -1;

unsigned long step_ts = 0;
unsigned long step_ms = 500;

void touchEvent(uint8_t id, TouchEvent event){
    // printer.printf("[%03i] touch event : %02i -> %s\n", millis()/10, id, touchEventNames[event]);
    // dispatch event to other functions depending on state
    switch (event){
        case TOUCH_DOWN:
            break;
        case TOUCH_UP:
            break;
        case SHORT_PRESS:
            // Serial.print("short press : ");Serial.println(id);
            if(live){
                updateUdpServer(id); 
                break;
                }else{
                setSequenceSP(id);
                updateUdpServer(id); 
                }
            break;
        case LONG_PRESS:
            // Serial.print("long press : ");Serial.println(id);
            setSequenceLP(id);
            updateUdpServer(id);
            break;
    }
}

void updateTouchSensor(TouchThis& touch, int idx){
    if(touchRead(touch.pinA) < TOUCH_DOWN_THRESH && touchRead(touch.pinB) < TOUCH_DOWN_THRESH){
        if(touch.state == false){
            touch.state = true;
            touch.touch_down_ts = millis();
            touchEvent(idx, TOUCH_DOWN);
            // Serial.print("idx : ");Serial.println(idx);
            if (live){
                touch.color = colors[idx];
            } else {
                touch.color = touchDownColor;
            }
        }
        if(touch.touch_down_ts+LONG_PRESS_MS < millis()){
            touch.color = longPressColor;
        }
    }
    else if(touchRead(touch.pinA) > TOUCH_UP_THRESH && touchRead(touch.pinB) > TOUCH_UP_THRESH){
        if(touch.state == true){
            touch.state = false;
            touch.color = touchUpColor;
            touchEvent(idx, TOUCH_UP);
            if(touch.touch_down_ts + LONG_PRESS_MS < millis()){
                touchEvent(idx, LONG_PRESS);
            }
            else if(touch.touch_down_ts + SHORT_PRESS_MS < millis()){
                touchEvent(idx, SHORT_PRESS);
            }
        }
    }
}

void setBrightness(int _b){
    FastLED.setBrightness(_b);
}

void setBpm(float _b){
    //Serial.println(_b);
    bpmParam.v = _b;
    //step_ms = (60000/_b); 
    step_ms = (60000/_b/4); // for 4 clicks per beat 
    Serial.print("bpm value set to : ");
    Serial.println(_b);
    Serial.print("step_ms : ");
    Serial.println(step_ms);
}

void setPlayState(int _b){
    //Serial.println(_b);
    playParam.v = _b;
    Serial.print("playParam value set to : ");
    Serial.println(_b);
}

void setCurVoice(int _b){
    curVoiceParam.v = _b;
    curVoice = curVoiceParam.v;
    Serial.print("curVoice value set to : ");
    Serial.println(_b);
}

void setMidiChannel(int _b){
    midiChannelParam.v = _b;
    devices[curVoice].chan = midiChannelParam.v;
    Serial.print("midi channel value set to : ");
    Serial.println(_b);
}

void setMidiNote(int _b){
    midiNoteParam.v = _b;
    devices[curVoice].note = midiNoteParam.v;
    Serial.print("midi note value set to : ");
    Serial.println(_b);
}

void setSequenceTDLF(const char * _b){
    Serial.print("incoming sequence is : ");
    Serial.println(_b);
}

void setSequenceSP(uint8_t id){
    devices[curVoice].steps[id+16*devices[curVoice].bar].on = !devices[curVoice].steps[id+16*devices[curVoice].bar].on;
    // Serial.print("devices[curVoice].steps[id+16*devices[curVoice].bar].on : ");Serial.println(devices[curVoice].steps[id+16*devices[curVoice].bar].on);
}

void setSequenceLP(uint8_t id){
    switch(id){
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7: 
            curVoice = id; // Setting the current Voice value
            stepOnColor = colors[curVoice];
            // Serial.print("curVoice : ");Serial.println(curVoice);
            break;
        case 8: // Setting the bar value (1-4)
            devices[curVoice].bar = (devices[curVoice].bar + 1) % 4;
            Serial.print("bar : ");Serial.println(devices[curVoice].bar);
            // Show 'animation', for steps[note][0].bar = 1 for instance : 0000 XXXX 0000 0000 
            for (int i = devices[curVoice].bar*4; i < (devices[curVoice].bar*4)+4; i++){
                leds[i] = stepColor;
                FastLED.show();
            }
            break;
        case 9: 
            break; // tbd
        case 10: 
            break; // tbd
        case 11: 
            break; // tdb
        case 12: 
            // set midi channel
            devices[curVoice].chan = (devices[curVoice].chan + 1) % 17;
            Serial.print("midi channel : ");Serial.println(devices[curVoice].chan);
            // Show 'animation', for midi channel for instance channel 9 : XXXX XXXX X000 0000 
            for (int i = 0; i < devices[curVoice].chan; i++){
                leds[i] = stepColor;
                FastLED.show();
            }
            break;

        case 13: // live mode
            live = !live;
            Serial.print("live mode : ");Serial.println(live);
            for (int i = 0; i < 8; i++){
                leds[i] = colors[i];
                FastLED.show();
            }
            break;
        case 14: 
            // mute sequence
            devices[curVoice].mute != devices[curVoice].mute;
            Serial.print("mute sequence state : ");Serial.println(devices[curVoice].mute);
            // anim : dim all leds by 50%
            break;
        case 15: // clear sequence
            for (int i = 0; i < STEP_COUNT; i++){  
                devices[curVoice].steps[i].on = 0;
            }
            Serial.println("clear sequence");
            break;
    }
}

void updateUdpServer(uint8_t id){
    if (live){
        String sequenceString = "/tdlf/live/";
        // sequenceString.concat("v:"); // voice value to the osc address
        // sequenceString.concat(id); // Add the voice value to the osc address
        // sequenceString.concat(",m:"); // mute state        
        // sequenceString.concat(steps[curVoice][0].mute); // Set the mute state
        // sequenceString.concat(",c:"); // midi Channel
        // sequenceString.concat(steps[curVoice][0].chan); // Send the midi Channel
        // sequenceString.concat(",b:"); // bar
        // sequenceString.concat(steps[curVoice][0].bar); // Send the bar
        // sequenceString.concat(",n:"); // note
        // sequenceString.concat(steps[curVoice][0].note); // Send the note
        // sequenceString.concat(",l:"); // note length
        // sequenceString.concat(steps[curVoice][0].length); // Send the note length
        myMicroOscUdp.sendInt(sequenceString.c_str(), 1);
    } else {
        char oscAddress[20]; // Adjust size as needed
        const char* oscAddressBase = "/tdlf/device/";
        snprintf(oscAddress, sizeof(oscAddress), "%s%d", oscAddressBase, curVoice); // Adding curVoice to the address
        myMicroOscUdp.sendMessage(oscAddress, "b", (char *)&devices[curVoice], sizeof(devices[curVoice]));
    }
}

void myBlobMessageParser( MicroOscMessage& receivedOscMessage) {  
    char monAddress[20];
    const uint8_t* blob;
    uint32_t length = receivedOscMessage.nextAsBlob(&blob);
    // Serial.println("blob received!");
    for (int i = 0 ; i < 8 ; i++ ){
        sprintf(monAddress, "/tdlf/device/%d", i);
        if ( receivedOscMessage.checkOscAddress(monAddress) ) {
            Serial.print("Received OSC message with address : /tdlf/device/");Serial.println(i);
            if( i == curVoice ){ // If the sent device is the same as curVoice  
                memcpy(&devices[curVoice], blob, sizeof(devices[curVoice])); // Update sequence 
                Serial.print("length : ");Serial.println(length);
                for ( int j = 0 ; j < 16 ; j++ ){
                    Serial.print("For j : "); Serial.println(j);
                    Serial.print("devices[curVoice].steps[j].on : "); Serial.println(devices[curVoice].steps[j].on);
                    
                    }
                // Serial.print("devices[curVoice].bar : ");Serial.println(devices[curVoice].bar);
            }
        }
    } 
}


void setup() {
    Serial.begin(9600);
    printer.begin();
    printer.println("[boot] booting");
    // FastLED.addLeds<APA102, LED_DATA_PIN, LED_CLK_PIN, RGB, DATA_RATE_MHZ(24)>(leds, NUM_LEDS);
    FastLED.addLeds<WS2812B, LED_DATA_PIN, GRB>(leds, NUM_LEDS);
    stepOnColor = colors[0]; // start value
    leds[0] = CRGB(10,0,0);
    FastLED.show();

    bpmParam.set("/abletonLink/bpm", 4.0, 240.0, 120.0);
    bpmParam.saveType = SAVE_ON_REQUEST;
    bpmParam.setCallback(setBpm);
    paramCollector.add(&bpmParam);
    
    playParam.set("/abletonLink/playState", 0, 2, 0);
    playParam.setCallback(setPlayState);
    paramCollector.add(&playParam);

    setStepParam.set("/abletonLink/curBeat", 0, 64, 0);
    setStepParam.setCallback([](int _s){ curBeat = _s; });
    paramCollector.add(&setStepParam);

    curVoiceParam.set("/tdlf/curVoice", 0, 7, 0);
    curVoiceParam.setCallback(setCurVoice);
    paramCollector.add(&curVoiceParam);

    midiChannelParam.set("/tdlf/midiChannel", 0, 16, 10);
    midiChannelParam.setCallback(setMidiChannel);
    paramCollector.add(&midiChannelParam);

    midiNoteParam.set("/tdlf/midiNote", 20, 127, 36);
    midiNoteParam.setCallback(setMidiNote);
    paramCollector.add(&midiNoteParam);

    // We need to track a blob format

 //////////////////////////
    // long int c = 255;//CRGB(255,0,0);
    // colorA.set("/color/a", c);
    // colorA.saveType = SAVE_ON_REQUEST;
    // paramCollector.add(&colorA);

    // colorB.set("/color/b", c);
    // colorB.saveType = SAVE_ON_REQUEST;
    // paramCollector.add(&colorB);

    brightnessParam.set("/leds/brightness", 0, 255, 200);
    brightnessParam.saveType = SAVE_ON_REQUEST;
    brightnessParam.setCallback(setBrightness);
    paramCollector.add(&brightnessParam);

    setupEsparam(&printer);
    // we need to make 
    setBpm(bpmParam.v);
    startNetwork();

    for(int i = 0; i < STEP_COUNT; i++){
        touchSensors[i].pinA = TOUCH_PINS[i][0];
        touchSensors[i].pinB = TOUCH_PINS[i][1];
        touchSensors[i].color = touchUpColor;
        touchSensors[i].state = false;
        touchSensors[i].touch_down_ts = 0;
    }

    for(int i = 0; i < NUM_LEDS; i++){
        leds[i] = CRGB(0,10,0); 
        FastLED.show();         
        delay(20);
    }

    ////// initiate values of the array
    for (int i = 0; i < 7; i++){  
        devices[curVoice].chan = 10;
        devices[curVoice].bar  = 0;
        devices[curVoice].note = 0;
        devices[curVoice].mute = 0;
    }

    if (!MDNS.begin("seq1")) {
        Serial.println("Error setting up MDNS responder!");
        while(1) {
            delay(1000);
        }
    }
  
    Serial.println("mDNS responder started");

    if (MDNS.queryService("tdlf", "udp")) {
        Serial.println("Service found!");
        IPAddress serviceIP = MDNS.IP(0); // Get the IP address of the service
        Serial.print("serviceIP ");Serial.println(serviceIP);
        char tdlfIP[32];
        serviceIP.toString().toCharArray(tdlfIP, sizeof(tdlfIP));
        Serial.print("tdlfIP ");Serial.println(tdlfIP);
        epConf.oscOutIpParam.set("/config/osc/out/ip", tdlfIP);
        paramCollector.add(&epConf.oscOutIpParam);
    }
    startNetwork(); // need to call this again so we send to te correct ip address?
    //updateEsparam(); // Where should I place this?

    // send a hello world message to the server
    delay(10000); // not necessary unless I could wait until 'startNetwork()' returns with a 'task completed' message
    myMicroOscUdp.sendInt("/tdlf/clientIP", 1);
    // Serial.println("hello message sent to the server"); // the server will respond with the current sequence if it exists
    setMessageNotRecognizedCallbackFunction(myBlobMessageParser);
}

////////////////////////////////////////////////////////////////////////////////////////////////
void loop() {
    updateEsparam();

    // update the oled screen and weblog
    // printer.update();
   
    if(playParam.v == 1 || playParam.v == 2){

    if(millis() >= step_ms + step_ts){ 
        curStep++; // If time between steps > step_ms increase currentStep right away as the udp sent curBeat is lagging behind
        step_ts = millis();
        }
        // Serial.print("curBeat:  ");Serial.println(curBeat); // incoming from the server

    if (curBeat != oldBeat){
  
       //curStep = curBeat; // Depends on WiFI, introduces 'wavering' 

        if (curBeat % 16 == 0) {
             
            if( curBar < devices[curVoice].bar ){ 
                curBar++;
                }else{ 
                    curBar = 0;
                }
            }

        if (curBeat == 0){ 
            curStep = curBeat; // take the cue from the abletonLink/curBeat every 64 steps
            curBar = 0;
            }

        curBar = curBar % 4;
        // Serial.print("curBar:  ");Serial.println(curBar);
        curStep = curStep % STEP_COUNT;
        // Serial.print("curStep:  ");Serial.println(curStep);
        // Serial.print("curBeat:  ");Serial.println(curBeat);
        oldBeat = curBeat;
        }
    }

    for(int i = 0; i < STEP_COUNT; i++){
        updateTouchSensor(touchSensors[i], i);
        if(curStep == i && curBar == devices[curVoice].bar){
            leds[i] = stepColor; // active step indicator
        } else if(touchSensors[i].state == true){
            leds[i] = touchSensors[i].color; // indicate short or long press
        } else if(devices[curVoice].steps[i+16*devices[curVoice].bar].on){
            leds[i] = stepOnColor;
        }
        else {
            leds[i] = stepOffColor;
        }
	
        if(devices[curVoice].mute){
            FastLED.setBrightness(25); // Goes red?
        } else {FastLED.setBrightness(127);} // How I include the param brightness value instea of hard-codng 127?
        // insert animations here? // fade_raw()?
        // calculate the values left and fade or 10 iterations?
    }
    // for(int i = 0; i < STEP_COUNT; i++){
    //     Serial.printf("(%02i:%02i) ", touchRead(TOUCH_PINS[i][0]), touchRead(TOUCH_PINS[i][1]));
    // }
    FastLED.show();
}